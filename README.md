## MODULE 4: pandas-challenge
### Created by: Melissa Acevedo

District Summary: Key metrics:

- Total number of unique schools
- Total students
- Total budget
- Average math score
- Average reading score
- % passing math (the percentage of students who passed math)
- % passing reading (the percentage of students who passed reading)
- % overall passing (the percentage of students who passed math AND reading)

School Summary: key metrics:

- School name
- School type
- Total students
- Total school budget 
- Per student budget
- Average math score
- Average reading score
- % passing math (the percentage of students who passed math)
- % passing reading (the percentage of students who passed reading)
- % overall passing (the percentage of students who passed math AND reading)

Highest-Performing Schools (by % Overall Passing)
- Sort the schools by % Overall Passing in descending order and display the top 5 rows.

Lowest-Performing Schools (by % Overall Passing)
- Sort the schools by % Overall Passing in ascending order and display the top 5 rows.

Math Scores by Grade
- DataFrame that lists the average math score for students of each grade level (9th, 10th, 11th, 12th) at each school.

Reading Scores by Grade
- DataFrame that lists the average reading score for students of each grade level (9th, 10th, 11th, 12th) at each school.

Scores by School Spending
- Dataframe with school performance based on average spending ranges (per student).

Scores by School Size
- DataFrame called size_summary that breaks down school performance based on school size (small, medium, or large).

Scores by School Type
- DataFrame that shows school performance based on the "School Type".